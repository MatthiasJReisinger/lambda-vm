#!/usr/bin/python

import argparse
import sys

from antlr4 import *
from LambdaLexer import LambdaLexer
from LambdaParser import LambdaParser

def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("file",
                           help="a file containing lambda expressions",
                           nargs="?")
    args = arg_parser.parse_args()

    if args.file == None:
        print("Lambda VM (version 0.1.0)")
        print("Type 'q' to quit")

        while True:
            input_string = input(">>> ")
            if input_string == "q":
                break
            elif len(input_string) > 0:
                evaluate_string(input_string)

        print("Goodbye!")
    else:
        evaluate_file(args.file)

def evaluate_string(lambda_str):
    input_stream = InputStream.InputStream(lambda_str)
    evaluate_stream(input_stream)

def evaluate_file(lambda_file):
    file_stream = FileStream(lambda_file)
    evaluate_stream(file_stream)

def evaluate_stream(stream):
    lambda_expression = parse(stream)
    print(lambda_expression.evaluate())

def parse(stream):
    lexer = LambdaLexer(stream)
    token_stream = CommonTokenStream(lexer)
    parser = LambdaParser(token_stream)
    ctx = parser.program()
    lambda_expression = ctx.result
    return lambda_expression

if __name__ == '__main__':
    main()
