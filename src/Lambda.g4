grammar Lambda;

options {
    language=Python3;
}

@header {
from ast import *
}

program returns [result]
    : lambda_expression ('\n')* {$result = $lambda_expression.result}
    ;

lambda_expression returns [result]
    : arithmetic_expression {$result = $arithmetic_expression.result}
    | application {$result = $application.result}
    | abstraction {$result = $abstraction.result}
    | '(' bracketed_lambda=lambda_expression ')'
      {$result = $bracketed_lambda.result}
    ;

arithmetic_expression returns [result]
    : left_op=arithmetic_expression '*' right_op=arithmetic_expression
      {$result = MulExpression($left_op.result, $right_op.result)}
    | left_op=arithmetic_expression '+' right_op=arithmetic_expression
      {$result = AddExpression($left_op.result, $right_op.result)}
    | left_op=arithmetic_expression '-' right_op=arithmetic_expression
      {$result = SubExpression($left_op.result, $right_op.result)}
    | '(' bracketed_expression=arithmetic_expression ')'
      {$result = $bracketed_expression.result}
    | term {$result = $term.result}
    ;

term returns [result]
    : IDENTIFIER {$result = Identifier($IDENTIFIER.text)}
    | INTEGER {$result = Integer($INTEGER.int)}
    ;

application returns [result]
    : abstraction lambda_expression
      {$result = Application($abstraction.result, $lambda_expression.result)}
    ;

abstraction returns [result]
    : '#' IDENTIFIER '.(' lambda_expression ')'
      {$result = Abstraction(Identifier($IDENTIFIER.text), $lambda_expression.result)}
    ;

IDENTIFIER
    : 'a'..'z'
    ;

INTEGER
    : '0'
    | '1'..'9' ('0'..'9')*
    ;

WHITESPACE
    : [ \t]+ -> skip
    ;


