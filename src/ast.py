from abc import *

class LambdaExpression(metaclass=ABCMeta):
    @abstractmethod
    def evaluate(self):
        """
        Evaluates the lambda expression and returns the
        resulting LambdaExpression instance.
        """
        pass

    @abstractmethod
    def replace(self, identifier, lambda_expression):
        """
        Replaces all occurrences of the identifier in the current
        lambda expression by the given lambda_expressions.
        identifer - ast.Identifer
        lambda_expression - ast.LambdaExpression
        """
        pass

    @abstractmethod
    def free(self):
        """
        Returns the set of unbound identifiers in the lambda
        expression.
        """

class Application(LambdaExpression):
    def __init__(self, abstraction, operand):
        self._abstraction = abstraction
        self._operand = operand

    def abstraction(self):
        return self._abstraction

    def operand(self):
        return self._operand

    def evaluate(self):
        abstraction_body = self.abstraction().body()
        abstraction_parameter = self.abstraction().identifier()
        new_body = abstraction_body.replace(abstraction_parameter, self.operand())
        return new_body.evaluate()

    def replace(self, identifier, lambda_expression):
        self._abstraction = self._abstraction.replace(identifier, lambda_expression)
        self._operand = self._operand.replace(identifier, lambda_expression)
        return self

    def free(self):
        return union(self.abstraction().free(), self.operand.free())

    def __str__(self):
        return str(self.abstraction()) + " " + str(self.operand())

class Abstraction(LambdaExpression):
    def __init__(self, identifier, body):
        self._identifier = identifier
        self._body = body

    def identifier(self):
        return self._identifier

    def body(self):
        return self._body

    def evaluate(self):
        return self

    def replace(self, identifier, lambda_expression):
        self._body = self._body.replace(identifier, lambda_expression)
        return self

    def free(self):
        return self.body().free() - {self.identifier()}

    def __str__(self):
        return "#" + str(self.identifier()) + ".(" + str(self.body()) + ")"

class ArithmeticExpression(LambdaExpression):
    pass

class BinaryArithmeticExpression(ArithmeticExpression):
    def __init__(self, left_operand=None, right_operand=None):
        self._left_operand = left_operand
        self._right_operand = right_operand

    def left_operand(self):
        return self._left_operand

    def right_operand(self):
        return self._right_operand

    def evaluate(self):
        left_result = self.left_operand().evaluate()
        right_result = self.right_operand().evaluate()

        if (not isinstance(left_result, Integer) or 
            not isinstance(right_result, Integer)):
            return self
        else:
            int_result = self.evaluate_binary(left_result.value(),
                                              right_result.value())
            return Integer(int_result)

    @abstractmethod
    def evaluate_binary(self, left_op, right_op):
        pass
    
    def replace(self, identifier, lambda_expression):
        self._left_operand = self._left_operand.replace(identifier,
                                                        lambda_expression)
        self._right_operand = self._right_operand.replace(identifier,
                                                          lambda_expression)
        return self

    def free(self):
        return union(self._left_operand.free(), self._right_operand.free())
        

class MulExpression(BinaryArithmeticExpression):
    def evaluate_binary(self, left_op, right_op):
        return left_op * right_op

    def __str__(self):
        return "(" + str(self.left_operand()) + ") " + "*" + " (" \
               + str(self.right_operand()) + ")"

class AddExpression(BinaryArithmeticExpression):
    def evaluate_binary(self, left_op, right_op):
        return left_op + right_op 

    def __str__(self):
        return "(" + str(self.left_operand()) + ") " + "+" + " (" \
               + str(self.right_operand()) + ")"

class SubExpression(BinaryArithmeticExpression):
    def evaluate_binary(self, left_op, right_op):
        return left_op - right_op

    def __str__(self):
        return "(" + str(self.left_operand()) + ") " + "-" + " (" \
               + str(self.right_operand()) + ")"

class Term(ArithmeticExpression):
    def evaluate(self):
        """
        Terms represent leafs in the abstract syntax tree and
        therefor need not be evaluated.
        """
        return self

class Integer(Term):
    def __init__(self, value):
        self._value = value

    def value(self):
        return self._value

    def replace(self, identifier, lambda_expression):
        return self

    def free(self):
        return {}

    def __str__(self):
        return str(self._value)

class Identifier(Term):
    def __init__(self, name):
        self._name = name

    def name(self):
        return self._name
    
    def replace(self, identifier, lambda_expression):
        if self.name() == identifier.name():
            return lambda_expression
        else:
            return self

    def free(self):
        return {self}

    def __str__(self):
        return self._name

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        return self._name == other._name

    def __ne__(self, other):
        return not self == other

