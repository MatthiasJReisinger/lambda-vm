
all: grammar

grammar: src/Lambda.g4
	antlr4 -Dlanguage=Python3 src/Lambda.g4

clean:
	rm -rf src/LambdaLexer.py src/LambdaLexer.tokens src/LambdaParser.py src/LambdaListener.py src/Lambda.tokens src/__pycache__

.PHONY: clean all
